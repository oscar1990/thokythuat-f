<?php
/**
 * The template for displaying header.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! hello_get_header_display() ) {
	return;
}

$is_editor = isset( $_GET['elementor-preview'] );
$site_name = get_bloginfo( 'name' );
$tagline   = get_bloginfo( 'description', 'display' );
$header_nav_menu = wp_nav_menu( [
	'theme_location' => 'menu-1',
	'fallback_cb' => false,
	'echo' => false,
] );
?>
<header id="site-header" class="site-header dynamic-header <?php echo esc_attr( hello_get_header_layout_class() ); ?>" role="banner">
	<div class="header-inner">
		<div class="site-branding block-logo show-<?php echo esc_attr( hello_elementor_get_setting( 'hello_header_logo_type' ) ); ?>">
			<div class="search-thokythuat">

                <input type="text">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11 19C15.4183 19 19 15.4183 19 11C19 6.58172 15.4183 3 11 3C6.58172 3 3 6.58172 3 11C3 15.4183 6.58172 19 11 19Z" stroke="#555555" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M21 21L16.65 16.65" stroke="#555555" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <?php if ( has_custom_logo() && ( 'title' !== hello_elementor_get_setting( 'hello_header_logo_type' ) || $is_editor ) ) : ?>
				<div class="site-logo <?php echo esc_attr( hello_show_or_hide( 'hello_header_logo_display' ) ); ?>">
					<?php the_custom_logo(); ?>
				</div>
			<?php endif;

			if ( $site_name && ( 'logo' !== hello_elementor_get_setting( 'hello_header_logo_type' ) || $is_editor ) ) : ?>
				<h1 class="site-title <?php echo esc_attr( hello_show_or_hide( 'hello_header_logo_display' ) ); ?>">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr_e( 'Home', 'hello-elementor' ); ?>" rel="home">
						<?php echo esc_html( $site_name ); ?>
					</a>
				</h1>
			<?php endif;

			if ( $tagline && ( hello_elementor_get_setting( 'hello_header_tagline_display' ) || $is_editor ) ) : ?>
				<p class="site-description <?php echo esc_attr( hello_show_or_hide( 'hello_header_tagline_display' ) ); ?>">
					<?php echo esc_html( $tagline ); ?>
				</p>
			<?php endif; ?>

            <div class="social">
                <svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M26.5 14H24.0455C22.9605 14 21.9199 14.4214 21.1527 15.1716C20.3856 15.9217 19.9545 16.9391 19.9545 18V20.4H17.5V23.6H19.9545V30H23.2273V23.6H25.6818L26.5 20.4H23.2273V18C23.2273 17.7878 23.3135 17.5843 23.4669 17.4343C23.6204 17.2843 23.8285 17.2 24.0455 17.2H26.5V14Z" fill="#333333"/>
                    <rect x="1" y="1" width="42" height="42" rx="21" stroke="#EDEDED" stroke-width="2"/>
                </svg>

                <svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M30.125 15.5073C29.4177 15.9967 28.6345 16.371 27.8057 16.6158C27.3608 16.1141 26.7696 15.7585 26.112 15.5971C25.4544 15.4357 24.7621 15.4763 24.1288 15.7134C23.4955 15.9505 22.9517 16.3726 22.571 16.9227C22.1902 17.4728 21.9909 18.1244 22 18.7892V19.5137C20.7019 19.5467 19.4157 19.2643 18.2559 18.6917C17.096 18.1191 16.0985 17.274 15.3523 16.2318C15.3523 16.2318 12.3977 22.7522 19.0455 25.6501C17.5243 26.6629 15.7121 27.1708 13.875 27.0991C20.5227 30.7216 28.6477 27.0991 28.6477 18.7675C28.647 18.5657 28.6273 18.3644 28.5886 18.1662C29.3425 17.437 29.8745 16.5163 30.125 15.5073Z" fill="#333333"/>
                    <rect x="1" y="1" width="42" height="42" rx="21" stroke="#EDEDED" stroke-width="2"/>
                </svg>

                <svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M25.5 15H18.5C16.567 15 15 16.567 15 18.5V25.5C15 27.433 16.567 29 18.5 29H25.5C27.433 29 29 27.433 29 25.5V18.5C29 16.567 27.433 15 25.5 15Z" stroke="#333333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M24.8 21.559C24.8864 22.1415 24.7869 22.7365 24.5156 23.2593C24.2444 23.782 23.8152 24.206 23.2891 24.4707C22.7631 24.7355 22.1669 24.8277 21.5855 24.7341C21.004 24.6406 20.4668 24.366 20.0504 23.9496C19.6339 23.5331 19.3594 22.996 19.2658 22.4145C19.1723 21.8331 19.2644 21.2369 19.5292 20.7108C19.794 20.1848 20.2179 19.7556 20.7407 19.4843C21.2635 19.2131 21.8584 19.1136 22.441 19.2C23.0352 19.2881 23.5854 19.565 24.0102 19.9898C24.435 20.4146 24.7119 20.9647 24.8 21.559Z" stroke="#333333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M25.85 18.1499H25.8586" stroke="#333333" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    <rect x="1" y="1" width="42" height="42" rx="21" stroke="#EDEDED" stroke-width="2"/>
                </svg>

                <svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M29.1862 20.7217C27.5722 24.2275 23.6761 29 21.2135 29C18.7866 29 18.4356 23.7376 17.1105 20.2352C16.4581 18.5116 16.0371 18.9074 14.8144 19.7776L14.0689 18.7998C15.8521 17.2059 17.6366 15.3545 18.733 15.2517C19.9677 15.1314 20.7277 15.9896 21.0119 17.8268C21.3873 20.2413 21.9121 23.989 22.8281 23.989C23.5419 23.989 25.3006 21.0161 25.3912 19.953C25.5518 18.3966 24.2656 18.3496 23.1493 18.8355C24.916 12.9528 32.2655 14.0361 29.1862 20.7217Z" fill="#333333"/>
                    <rect x="1" y="1" width="42" height="42" rx="21" stroke="#EDEDED" stroke-width="2"/>
                </svg>

            </div>
		</div>

		<?php if ( $header_nav_menu ) : ?>
			<nav class="site-navigation <?php echo esc_attr( hello_show_or_hide( 'hello_header_menu_display' ) ); ?>" role="navigation">
				<?php
				// PHPCS - escaped by WordPress with "wp_nav_menu"
				echo $header_nav_menu; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				?>
			</nav>
			<div class="site-navigation-toggle-holder <?php echo esc_attr( hello_show_or_hide( 'hello_header_menu_display' ) ); ?>">
				<div class="site-navigation-toggle">
					<i class="eicon-menu-bar"></i>
					<span class="elementor-screen-only">Menu</span>
				</div>
			</div>
			<nav class="site-navigation-dropdown <?php echo esc_attr( hello_show_or_hide( 'hello_header_menu_display' ) ); ?>" role="navigation">
				<?php
				// PHPCS - escaped by WordPress with "wp_nav_menu"
				echo $header_nav_menu; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				?>
			</nav>
		<?php endif; ?>
	</div>
</header>
